package config

type Psc struct {
	Url          string
	RedirectUrl  string
	ClientID     string
	ClientSecret string
	Scope        string
}

type Hub struct {
	Url          string
	CloudUrl     string
	ClientID     string
	ClientSecret string
}

type Signature struct {
	ReturnType                 string
	HashAlgorithm              string
	Profile                    string
	SignatureFormat            string
	OperationType              string
	IncludeXPathEnveloped      bool
	GenerateSimplifiedXMLDSign bool
	IncludeSigningTime         bool
	CanonicalizerType          string
	LineBreak                  bool
	GenerationID               string
	Node                       *SpecificNode
}

type SpecificNode struct {
	ID        string
	Name      string
	Namespace string
}

var Config = struct {
	Port      int
	Hub       Hub
	Psc       Psc
	Signature Signature
}{
	Port: 8000,

	Signature: Signature{
		ReturnType:                 "LINK",   // LINK, BASE64
		HashAlgorithm:              "SHA256", // SHA256, SHA512
		Profile:                    "BASIC",  // BASIC, COMPLETE, ADRB, ADRT, ADRV, ADRC, ADRA, ETSI_B, ETSI_T, ETSI_LT, ETSI_LTA
		SignatureFormat:            "ENVELOPED",
		OperationType:              "SIGNATURE", // Opcional. SIGNATURE, CO_SIGNATURE
		IncludeXPathEnveloped:      false,       // Opcional
		GenerateSimplifiedXMLDSign: false,       // Opcional
		IncludeSigningTime:         false,       // Opcional
		CanonicalizerType:          "",          // Opcional. INCLUSIVE, EXCLUSIVE
		LineBreak:                  true,        // Opcional
		GenerationID:               "",          // Opcional. SEQUENCIAL, PADRONIZADO
		Node: &SpecificNode{
			ID:        "", // Opcional
			Name:      "", // Opcional
			Namespace: "", // Opcional
		}, // Opcional
	},

	Hub: Hub{
		Url:          "<HUB_URL>",   // Ex: Produção: https://hub2.bry.com.br, Homologação: https://hub2.hom.bry.com.br
		CloudUrl:     "<CLOUD_URL>", // Ex: Produção: https://cloud.bry.com.br, Homologação: https://cloud-hom.bry.com.br
		ClientID:     "<CLOUD_CLIENT_ID>",
		ClientSecret: "<CLOUD_CLIENT_SECRET>",
	},

	Psc: Psc{
		Url:          "<PSC_URL>",      // Verifique com seu provedor de acesso ao certificado em nuvem sua URL base
		RedirectUrl:  "<REDIRECT_URL>", // Ex: http://localhost:8000/psc
		ClientID:     "<PSC_CLIENT_ID>",
		ClientSecret: "<PSC_CLIENT_SECRET>",
		Scope:        "signature_session", // Valores disponíveis: single_signature | multi_signature | signature_session
	},
}
