package main

import (
	"log"
	"xml/psc/config"
	"xml/psc/internal/hub"
	"xml/psc/internal/memory"
	"xml/psc/internal/psc"
	"xml/psc/internal/web"
)

func main() {
	log.Println("Initializing http service")
	c := config.Config
	mem := memory.New()
	p := psc.New(c.Psc, mem)
	h, err := hub.New(c.Hub, c.Signature)
	if err != nil {
		panic(err)
	}

	err = web.Start(c.Port, h, p)
	log.Println("Error on web service: ", err)
}
