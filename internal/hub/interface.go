package hub

type Hub interface {
	XmlSignature(document []byte, pscUri, pscToken string) (signedDocument []byte, err error)
}
