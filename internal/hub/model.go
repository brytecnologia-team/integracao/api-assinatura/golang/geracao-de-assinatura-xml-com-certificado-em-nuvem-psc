package hub

import "encoding/json"

type kmsData struct {
	Url   string `json:"url"`
	Token string `json:"token"`
}

func newKmsDataString(url string, token string) string {
	data := kmsData{
		Url:   url,
		Token: token,
	}
	b, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}
	return string(b)
}

type base64Response []string

type linkResponse struct {
	Documentos []struct {
		Links []struct {
			Href string `json:"href"`
		} `json:"links"`
	} `json:"documentos"`
}

type tokenResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}
