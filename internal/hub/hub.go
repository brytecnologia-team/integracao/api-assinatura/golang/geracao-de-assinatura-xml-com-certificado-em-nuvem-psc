package hub

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"xml/psc/config"
)

type hub struct {
	uri string
	tkn *token

	s config.Signature
}

func New(c config.Hub, s config.Signature) (Hub, error) {
	tkn, err := newToken(c.CloudUrl, c.ClientID, c.ClientSecret)
	if err != nil {
		return nil, err
	}

	return hub{
		uri: c.Url,
		tkn: tkn,
		s:   s,
	}, nil
}

func (h hub) XmlSignature(document []byte, pscUrl, pscToken string) ([]byte, error) {
	// URL para acesso a API
	uri := h.uri + "/api/xml-signature-service/v1/signatures/kms"

	// Multipart body writer
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Adiciona documento. Para assinaturas em lote, o valor do indice deve ser incrementado (originalDocuments[1][content], originalDocuments[2][content], ...)
	w, err := writer.CreateFormFile("originalDocuments[0][content]", "documento.xml")
	if err != nil {
		return nil, err
	}
	_, err = w.Write(document)
	if err != nil {
		return nil, err
	}

	// Adiciona parâmetros obrigatórios
	err = writer.WriteField("nonce", "1")
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("kms_data", newKmsDataString(pscUrl, pscToken))
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("returnType", h.s.ReturnType)
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("hashAlgorithm", h.s.HashAlgorithm)
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("profile", h.s.Profile)
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("signatureFormat", h.s.SignatureFormat)
	if err != nil {
		return nil, err
	}

	// Adiciona parâmetros opcionais
	if opType := h.s.OperationType; opType != "" {
		err = writer.WriteField("operationType", opType)
		if err != nil {
			return nil, err
		}
	}
	if cnType := h.s.CanonicalizerType; cnType != "" {
		err = writer.WriteField("canonicalizerType", cnType)
		if err != nil {
			return nil, err
		}
	}
	if gnType := h.s.GenerationID; gnType != "" {
		err = writer.WriteField("generationID", gnType)
		if err != nil {
			return nil, err
		}
	}
	err = writer.WriteField("includeXPathEnveloped", strconv.FormatBool(h.s.IncludeXPathEnveloped))
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("generateSimplifiedXMLDSig", strconv.FormatBool(h.s.GenerateSimplifiedXMLDSign))
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("includeSigningTime", strconv.FormatBool(h.s.IncludeSigningTime))
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("lineBreak", strconv.FormatBool(h.s.LineBreak))
	if err != nil {
		return nil, err
	}

	// Adiciona configuracoes de nodo, se presentes
	if node := h.s.Node; node != nil {
		// Para assinaturas em lote, o valor do indice deve ser incrementado (originalDocuments[1][specificNode], originalDocuments[2][specificNode], ...)
		prefix := "originalDocuments[0][specificNode]"

		if node.ID != "" {
			fieldName := prefix + "[id]"
			err = writer.WriteField(fieldName, node.ID)
			if err != nil {
				return nil, err
			}
		}

		if node.Name != "" {
			fieldName := prefix + "[name]"
			err = writer.WriteField(fieldName, node.Name)
			if err != nil {
				return nil, err
			}
		}

		if node.Namespace != "" {
			fieldName := prefix + "[namespace]"
			err = writer.WriteField(fieldName, node.Namespace)
			if err != nil {
				return nil, err
			}
		}
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		return nil, err
	}
	// Pega token JWT utilizado para requisição
	accessToken, err := h.tkn.getToken()
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", accessToken)
	req.Header.Set("kms_type", "PSC")
	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.Body == nil {
		return nil, fmt.Errorf("hub response body returned nil and status %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		// Ocorreu algum erro na requisição
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, fmt.Errorf("error reading hub response body: %s", err.Error())
		}
		return nil, errors.New(string(b))

	}

	var signedDocument []byte
	switch h.s.ReturnType {
	case "BASE64":
		// Decodifica resposta Base64 da API
		var response base64Response
		err = json.NewDecoder(resp.Body).Decode(&response)
		if err != nil {
			return nil, err
		}
		signedDocument, err = base64.StdEncoding.DecodeString(response[0])
		if err != nil {
			return nil, err
		}
	default: // "LINK"
		// Decodifica resposta Link da API, buscando o documento assinado
		var response linkResponse
		err = json.NewDecoder(resp.Body).Decode(&response)
		if err != nil {
			return nil, err
		}
		link := response.Documentos[0].Links[0]

		r, err := http.Get(link.Href)
		if err != nil {
			return nil, err
		}
		if r.StatusCode != http.StatusOK || r.Body == nil {
			return nil, fmt.Errorf("error getting response from link. Status '%d', has body? %t", r.StatusCode, r.Body != nil)
		}
		signedDocument, err = io.ReadAll(r.Body)
		if err != nil {
			return nil, err
		}
	}
	return signedDocument, nil
}
