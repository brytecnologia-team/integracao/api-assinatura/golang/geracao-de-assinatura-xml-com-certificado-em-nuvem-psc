package psc

type Psc interface {
	AccessCodeUri(id, state string) (redirectUri string, err error)
	AccessToken(id, code string) (accessToken string, err error)
	GetHost() string
}
